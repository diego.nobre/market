import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorageReference, AngularFireStorage } from '@angular/fire/storage';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  collection: string;
  constructor(private db: AngularFirestore, private afAuth: AngularFireAuth, private storage: AngularFireStorage) {}

  public setCollection(name: string) {
    this.collection = name;
  }

  async add(document: any) {
    document.creator = await this.afAuth.currentUser.then((user) => user.uid);
    const imageData = document.img;
    delete document.image;

    let documentId = null;
    let storageRef: AngularFireStorageReference = null;

    console.log(`api.add(${this.collection})`, document);
    return this.db
      .collection(this.collection)
      .add(document)
      .then((ref) => {
        console.log('ref: ', ref);
        documentId = ref.id;
        storageRef = this.storage.ref(`${this.collection}/${documentId}`);
        const uploadTask = storageRef.putString(imageData, 'base64', { contentType: 'image/png' });
        return uploadTask;
      })
      .then((task) => {
        console.log('task: ', task);
        return storageRef.getDownloadURL().toPromise();
      })
      .then((imageUrl) => {
        console.log('got url', imageUrl);
        return this.db.doc(`${this.collection}/${documentId}`).update({ img: imageUrl });
      });
  }

  async list() {
    const userId = await this.afAuth.currentUser.then((user) => user.uid);
    return this.db
      .collection(this.collection, (ref) => ref.where('creator', '==', userId))
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, data };
          })
        )
      );
  }

  delete(id: any) {
    this.db.doc(`${this.collection}/${id}`).delete();
    this.storage
      .ref(`${this.collection}/${id}`)
      .delete()
      .subscribe((res) => {
        console.log(`api.delete(${this.collection})`, res);
      });
  }

  async listAll() {
    console.log('api.listAll()', this.collection);

    return this.db
      .collection(this.collection)
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, data };
          })
        )
      );
  }

  get(id: any) {
    return this.db.doc(`${this.collection}/${id}`).valueChanges();
  }
}
