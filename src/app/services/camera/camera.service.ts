import { Injectable } from '@angular/core';

import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
const { Camera } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class CameraService {
  constructor() {}

  async takePhoto() {
    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      source: CameraSource.Camera,
    });

    return image.base64String;
    // const blobData = this.b64toBlob(image.base64String, `image/${image.format}`);
    // const fileName = Date.now().toString() + `image/${image.format}`;

    // return this.uploadImage(fileName, blobData);
  }

  async uploadFile() {
    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      source: CameraSource.Photos,
    });

    return image.base64String;
    // const blobData = this.b64toBlob(image.base64String, `image/${image.format}`);
    // const fileName = Date.now().toString() + `image/${image.format}`;

    // return this.uploadImage(fileName, blobData);
  }

  /**
   * @see https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
   *
   * @param b64Data
   * @param contentType
   * @param sliceSize
   */
  b64toBlob(b64Data: any, contentType: string = 'image/png', sliceSize: number = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  uploadImage(name: string = Date.now().toString() + '.png', image: Blob) {}
}
