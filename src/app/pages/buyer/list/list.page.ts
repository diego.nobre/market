import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Observable, BehaviorSubject } from 'rxjs';

import { FirebaseService } from 'src/app/services/firebase/firebase.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { ModalController } from '@ionic/angular';
import { CartModalPage } from '../../cart-modal/cart-modal.page';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class BuyerListPage implements OnInit {
  collection = 'products';

  documents: Observable<any>;
  cartItemCount: BehaviorSubject<number> = this.cartService.count();

  constructor(
    private authService: AuthService,
    private firebaseService: FirebaseService,
    private cartService: CartService,
    private modalCtrl: ModalController
  ) {
    this.firebaseService.setCollection(this.collection);
  }

  ngOnInit() {
    this.firebaseService.listAll().then((data) => {
      this.documents = data;
      console.log(this.documents);
    });
  }

  signOut() {
    this.authService.signOut();
  }

  async openCart() {
    const modal = await this.modalCtrl.create({
      component: CartModalPage,
      cssClass: 'cart-modal',
    });
    modal.present();
  }
}
