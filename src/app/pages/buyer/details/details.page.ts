import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase/firebase.service';
import { CartService } from 'src/app/services/cart/cart.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class BuyerListDetailsPage implements OnInit {
  collection = 'products';
  id = null;
  document = null;
  amount = 0;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private firebaseService: FirebaseService,
    private cartService: CartService
  ) {
    this.firebaseService.setCollection(this.collection);
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.firebaseService.get(this.id).subscribe((res) => {
      console.log('document: ', res);
      this.document = res;
      this.document.id = this.id;
      this.amount = this.cartService.quantity(this.id);
    });

    this.cartService.get().subscribe((cart) => {
      console.log('cart', cart);
      this.amount = this.cartService.quantity(this.id);
    });
  }

  signOut() {
    this.authService.signOut();
  }

  add() {
    this.cartService.add(this.document);
  }

  remove() {
    this.cartService.decrease(this.document);
  }
}
