import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuyerListDetailsPageRoutingModule } from './details-routing.module';

import { BuyerListDetailsPage } from './details.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, BuyerListDetailsPageRoutingModule],
  declarations: [BuyerListDetailsPage],
})
export class BuyerListDetailsPageModule {}
