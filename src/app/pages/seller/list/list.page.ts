import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FirebaseService } from 'src/app/services/firebase/firebase.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class SellerListPage implements OnInit {
  collection = 'products';
  documents: Observable<any>;

  constructor(private authService: AuthService, private firebaseService: FirebaseService) {
    this.firebaseService.setCollection(this.collection);
  }

  ngOnInit() {
    this.firebaseService.list().then((data) => {
      this.documents = data;
      console.log(this.documents);
    });
  }

  signOut() {
    this.authService.signOut();
  }

  delete(id: any) {
    this.firebaseService.delete(id);
  }
}
