import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';

import { FirebaseService } from 'src/app/services/firebase/firebase.service';
import { CameraService } from 'src/app/services/camera/camera.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class SellerListDetailsPage implements OnInit {
  collection = 'products';
  form: FormGroup;
  images: any[] = [];

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private camera: CameraService,
    private authService: AuthService,
    private firebaseService: FirebaseService
  ) {
    this.firebaseService.setCollection(this.collection);
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      desc: ['', Validators.required],
      img: '',
    });
  }

  create() {
    this.firebaseService.add(this.form.value).then((res) => {
      this.navCtrl.pop();
    });
  }

  selectImage() {
    this.camera.uploadFile().then((data) => {
      console.log(data);
      this.images.push(`data:image/jpeg;base64,${data}`);
      // this.images.push(data);
      this.form.patchValue({ img: data });
    });
  }

  signOut() {
    this.authService.signOut();
  }
}
