import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-cart-modal',
  templateUrl: './cart-modal.page.html',
  styleUrls: ['./cart-modal.page.scss'],
})
export class CartModalPage implements OnInit {
  cart = [];
  checkoutFormatted = '';
  checkoutTotal: string;

  constructor(private cartService: CartService, private modalCtrl: ModalController) {}

  ngOnInit() {
    this.cartService.get().subscribe((res) => {
      this.cart = res;
    });
    this.checkout();
  }

  decrease(product) {
    this.cartService.decrease(product);
    this.checkout();
  }

  increase(product) {
    this.cartService.add(product);
    this.checkout();
  }

  remove(product) {
    this.cartService.remove(product);
    this.checkout();
  }

  total() {
    this.checkoutTotal = this.cart.reduce((i, j) => i + j.price * j.amount, 0);
    return this.checkoutTotal;
  }

  close() {
    this.modalCtrl.dismiss();
  }

  checkout() {
    console.log(this.cart);
    this.checkoutFormatted = 'https://api.whatsapp.com/send?phone=5583988205707&text=*Checkout summary*';
    this.cart.forEach((item) => {
      console.log(item);
      this.checkoutFormatted += '%0D%0A' + `- ${item.amount}x ${item.name} (R$ ${item.price})`;
    });

    this.checkoutFormatted += '%0D%0A%0D%0A *Total*: R$ ' + this.total();
  }
}
