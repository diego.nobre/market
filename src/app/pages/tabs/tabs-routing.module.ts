import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () => import('../home/home.module').then((m) => m.HomePageModule),
          },
        ],
      },
      {
        path: 'category',
        children: [
          {
            path: '',
            loadChildren: () => import('../category/category.module').then((m) => m.CategoryPageModule),
          },
        ],
      },
      {
        path: 'order',
        children: [
          {
            path: 'list',
            loadChildren: () => import('../buyer/list/list.module').then((m) => m.BuyerListPageModule),
          },
          {
            path: 'list/:id',
            loadChildren: () => import('../buyer/details/details.module').then((m) => m.BuyerListDetailsPageModule),
          },
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () => import('../profile/profile.module').then((m) => m.ProfilePageModule),
          },
        ],
      },
      {
        path: '',
        redirectTo: '/app/tabs/home',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
