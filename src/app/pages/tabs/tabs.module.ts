import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { HomePageModule } from '../home/home.module';
import { CategoryPageModule } from '../category/category.module';
import { BuyerListPageModule } from '../buyer/list/list.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsPageRoutingModule,
    // tabs pages
    HomePageModule,
    CategoryPageModule,
    BuyerListPageModule,
  ],
  declarations: [TabsPage],
})
export class TabsPageModule {}
