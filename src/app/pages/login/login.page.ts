import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  registerForm: FormGroup;
  loginForm: FormGroup;

  @ViewChild('flipContainer', { static: false }) flipContainer: ElementRef;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private router: Router
  ) {}

  ngOnInit() {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      name: ['', Validators.required],
      role: ['BUYER', Validators.required],
    });

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  navigateByRole(role) {
    if (role.toUpperCase() === 'BUYER') {
      this.router.navigateByUrl('/buyer');
    } else if (role.toUpperCase() === 'SELLER') {
      this.router.navigateByUrl('/seller');
    }
  }

  async login() {
    const loading = await this.loadingCtrl.create({
      message: 'Loading...',
    });
    await loading.present();

    this.authService.signIn(this.loginForm.value).subscribe(
      (user) => {
        loading.dismiss();
        console.log('after login', user);
        this.navigateByRole(user.role);
      },
      async (err) => {
        loading.dismiss();
        const alert = await this.alertCtrl.create({
          header: 'Error',
          message: err.message,
          buttons: ['OK'],
        });
        alert.present();
      }
    );
  }

  async register() {
    const loading = await this.loadingCtrl.create({
      message: 'Loading...',
    });
    await loading.present();

    this.authService.signUp(this.registerForm.value).then(
      async (res) => {
        await loading.dismiss();

        const toast = await this.toastCtrl.create({
          duration: 3000,
          message: 'Successfully created new Account!',
        });
        toast.present();
        this.navigateByRole(this.registerForm.get('role').value);

        console.log('finished: ', res);
      },
      async (err) => {
        loading.dismiss();
        const alert = await this.alertCtrl.create({
          header: 'Error',
          message: err.message,
          buttons: ['OK'],
        });
        alert.present();
      }
    );
  }

  toggleRegister() {
    this.flipContainer.nativeElement.classList.toggle('flip');
  }
}
