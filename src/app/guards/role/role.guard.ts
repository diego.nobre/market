import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const expectedRole = route.data.role;

    return this.authService.user.pipe(
      take(1),
      map((user) => {
        if (!user) {
          return false;
        } else {
          const role = user['role'];
          if (expectedRole === role) {
            return true;
          } else {
            this.router.navigateByUrl('/');
            this.authService.signOut();
            return false;
          }
        }
      })
    );
  }
}
