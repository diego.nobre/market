import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { RoleGuard } from './guards/role/role.guard';
import { AutomaticLoginGuard } from './guards/automatic-login/automatic-login.guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['/']);

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/login/login.module').then((m) => m.LoginPageModule),
    canActivate: [AutomaticLoginGuard],
  },
  {
    path: 'buyer',
    canActivate: [AngularFireAuthGuard, RoleGuard],
    data: {
      authGuardPipe: redirectUnauthorizedToLogin,
      role: 'BUYER',
    },
    children: [
      {
        path: 'list',
        loadChildren: () => import('./pages/buyer/list/list.module').then((m) => m.BuyerListPageModule),
      },
      {
        path: 'list/:id',
        loadChildren: () => import('./pages/buyer/details/details.module').then((m) => m.BuyerListDetailsPageModule),
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'seller',
    canActivate: [AngularFireAuthGuard, RoleGuard],
    data: {
      authGuardPipe: redirectUnauthorizedToLogin,
      role: 'SELLER',
    },
    children: [
      {
        path: 'list',
        loadChildren: () => import('./pages/seller/list/list.module').then((m) => m.SellerListPageModule),
      },
      {
        path: 'list/new',
        loadChildren: () => import('./pages/seller/details/details.module').then((m) => m.SellerListDetailsPageModule),
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'cart-modal',
    loadChildren: () => import('./pages/cart-modal/cart-modal.module').then((m) => m.CartModalPageModule),
  },
  {
    path: 'app',
    loadChildren: () => import('./pages/tabs/tabs.module').then((m) => m.TabsPageModule),
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then((m) => m.ProfilePageModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
