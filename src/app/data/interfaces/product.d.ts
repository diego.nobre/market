export interface Product {
  uid: string;
  name: string;
  price: number;
  desc: string;
  img: string;
}
